package com.linh.datastoredemo

import android.content.Context
import com.linh.datastoredemo.SettingSerializer.settingsDataStore
import kotlinx.coroutines.flow.map

class ProtoDataSource(private val context: Context) {
    suspend fun incrementCounter() {
        context.settingsDataStore.updateData { currentSettings ->
            currentSettings.toBuilder().setExampleCounter(currentSettings.exampleCounter + 1).build()
        }
    }

    fun getCounter() =
        context.settingsDataStore.data.map { value -> value.exampleCounter }
}