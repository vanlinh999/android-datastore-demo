package com.linh.datastoredemo

import android.content.Context
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import java.io.File

val datastoreModule = module {
    factory { PreferenceDataSource(get()) }
    factory { ProtoDataSource(get()) }

    viewModel { MainViewModel(get(), get()) }
}