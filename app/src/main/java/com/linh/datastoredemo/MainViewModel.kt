package com.linh.datastoredemo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber

class MainViewModel(private val preferenceDataSource: PreferenceDataSource, private val protoDataSource: ProtoDataSource) : ViewModel() {
    val input = MutableLiveData<String>()

    private val _data = MutableLiveData<String>()
    val data : LiveData<String> get() = _data

    private val _count = MutableLiveData<Int>()
    val count : LiveData<Int> get() = _count

    init {
        fetchData()
        readCount()
    }

    fun saveData() {
        viewModelScope.launch {
            preferenceDataSource.saveData(input.value ?: "")
            protoDataSource.incrementCounter()
        }
    }

    private fun fetchData() {
        viewModelScope.launch {
            preferenceDataSource.getData().collect {
                Timber.d("VM data $it")
                _data.value = it
            }
        }
    }

    private fun readCount() {
        viewModelScope.launch {
            protoDataSource.getCounter().collect {
                _count.value = it
            }
        }
    }
}