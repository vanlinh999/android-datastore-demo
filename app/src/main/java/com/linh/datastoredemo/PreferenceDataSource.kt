package com.linh.datastoredemo

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.io.File

class PreferenceDataSource(private val context: Context) {
    private val Context.testDatastore: DataStore<Preferences> by preferencesDataStore("message")

    private val TEST_VALUE_KEY = androidx.datastore.preferences.core.stringPreferencesKey("testValue")

    suspend fun saveData(data: String) {
        context.testDatastore.edit {
            it[TEST_VALUE_KEY] = data
        }
    }

    fun getData() : Flow<String> {
        return context.testDatastore.data.map { value ->
            value[TEST_VALUE_KEY] ?: ""
        }
    }
}