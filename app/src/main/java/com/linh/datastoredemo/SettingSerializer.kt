package com.linh.datastoredemo

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.dataStore
import com.google.protobuf.InvalidProtocolBufferException
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.InputStream
import java.io.OutputStream
import java.lang.Exception

object SettingSerializer : Serializer<SettingsPreferences> {
    override val defaultValue: SettingsPreferences = SettingsPreferences.getDefaultInstance()

    override suspend fun readFrom(input: InputStream): SettingsPreferences {
        return try {
            SettingsPreferences.parseFrom(input)
        } catch (e: InvalidProtocolBufferException) {
            Timber.e(e)
            defaultValue
        }
    }

    override suspend fun writeTo(t: SettingsPreferences, output: OutputStream) {
        return t.writeTo(output)
    }

    val Context.settingsDataStore: DataStore<SettingsPreferences> by dataStore(
        fileName = "settings.proto",
        serializer = SettingSerializer
    )
}